package com.joeyorlando.boot.grpc.client.autoconfigure;

import brave.Span;
import brave.Tracer;
import com.joeyorlando.boot.grpc.client.ClientProperties;
import com.joeyorlando.boot.grpc.client.customizer.InstrumentedChannelFactoryCustomizer;
import com.joeyorlando.boot.grpc.client.customizer.TracingChannelFactoryCustomizer;
import com.joeyorlando.boot.grpc.client.interceptors.TracingClientInterceptor;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Auto-configuration for {@link TracingClientInterceptor} to serialize the current
 * {@link Tracer} context into request {@link io.grpc.Metadata}.
 */
@Configuration
@ConditionalOnClass({Tracer.class, Span.class})
@AutoConfigureBefore(GrpcClientAutoconfig.class)
@AutoConfigureAfter(MetricsGrpcClientAutoconfig.class)
public class TracingGrpcClientAutoconfig {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(TracingGrpcClientAutoconfig.class);

    @Bean
    public TracingClientInterceptor tracingClientInterceptor() {
        LOG.info("Registering TracingClientInterceptor");
        return new TracingClientInterceptor();
    }

    @Bean
    public TracingChannelFactoryCustomizer tracingManagedChannelFactoryCustomizer(
            @Autowired(required = false) InstrumentedChannelFactoryCustomizer instrumentedCustomizer, ClientProperties clientProperties) {
        return new TracingChannelFactoryCustomizer(instrumentedCustomizer, clientProperties);
    }
}
