package com.joeyorlando.boot.grpc.client;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * gRPC Client Properties
 */
@Configuration
@ConfigurationProperties(prefix = ClientProperties.PREFIX)
public class ClientProperties {
    public static final String PREFIX = "grpc.client";
    private String serverAddress = "localhost";
    private Integer serverPort = 10000;
    private String serverTarget;
    private int minThreads = 10;
    private int maxThreads = 30;
    private Metrics metrics;

    public ClientProperties() {
        this.metrics = new Metrics();
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public Integer getServerPort() {
        return serverPort;
    }

    public void setServerPort(Integer serverPort) {
        this.serverPort = serverPort;
    }

    public String getServerTarget() {
        return serverTarget;
    }

    public void setServerTarget(String serverTarget) {
        this.serverTarget = serverTarget;
    }

    public int getMinThreads() {
        return minThreads;
    }

    public void setMinThreads(int minThreads) {
        this.minThreads = minThreads;
    }

    public int getMaxThreads() {
        return maxThreads;
    }

    public void setMaxThreads(int maxThreads) {
        this.maxThreads = maxThreads;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public void setMetrics(Metrics metrics) {
        this.metrics = metrics;
    }

    public static class Metrics {
        private ActiveThreadGauge activeThreadGauge;
        private CoreThreadGauge coreThreadGauge;
        private CurrentThreadGauge currentThreadGauge;
        private MaxThreadGauge maxThreadGauge;
        private ServiceDurationTimer serviceDurationTimer;

        public Metrics() {
            this.activeThreadGauge = new ActiveThreadGauge();
            this.coreThreadGauge = new CoreThreadGauge();
            this.currentThreadGauge = new CurrentThreadGauge();
            this.maxThreadGauge = new MaxThreadGauge();
            this.serviceDurationTimer = new ServiceDurationTimer();
        }

        public ActiveThreadGauge getActiveThreadGauge() {
            return activeThreadGauge;
        }

        public void setActiveThreadGauge(ActiveThreadGauge activeThreadGauge) {
            this.activeThreadGauge = activeThreadGauge;
        }

        public CoreThreadGauge getCoreThreadGauge() {
            return coreThreadGauge;
        }

        public void setCoreThreadGauge(CoreThreadGauge coreThreadGauge) {
            this.coreThreadGauge = coreThreadGauge;
        }

        public CurrentThreadGauge getCurrentThreadGauge() {
            return currentThreadGauge;
        }

        public void setCurrentThreadGauge(CurrentThreadGauge currentThreadGauge) {
            this.currentThreadGauge = currentThreadGauge;
        }

        public MaxThreadGauge getMaxThreadGauge() {
            return maxThreadGauge;
        }

        public void setMaxThreadGauge(MaxThreadGauge maxThreadGauge) {
            this.maxThreadGauge = maxThreadGauge;
        }

        public ServiceDurationTimer getServiceDurationTimer() {
            return serviceDurationTimer;
        }

        public void setServiceDurationTimer(ServiceDurationTimer serviceDurationTimer) {
            this.serviceDurationTimer = serviceDurationTimer;
        }

        public static class ServiceDurationTimer implements MetricProperties {
            private boolean enabled = true;
            private String name = "grpc.server.service.duration";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public static class ActiveThreadGauge implements ThreadGaugeProperties {
            private boolean enabled = true;
            private String name = "grpc.server.threads.active";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public static class CoreThreadGauge implements ThreadGaugeProperties {
            private boolean enabled = true;
            private String name = "grpc.server.threads.core";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public static class CurrentThreadGauge implements ThreadGaugeProperties {
            private boolean enabled = true;
            private String name = "grpc.server.threads.current";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public static class MaxThreadGauge implements ThreadGaugeProperties {
            private boolean enabled = true;
            private String name = "grpc.server.threads.max";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public interface ThreadGaugeProperties extends MetricProperties {
        }

        public interface MetricProperties {
            boolean isEnabled();
            void setEnabled(boolean enabled);
            String getName();
            void setName(String name);
        }
    }
}
