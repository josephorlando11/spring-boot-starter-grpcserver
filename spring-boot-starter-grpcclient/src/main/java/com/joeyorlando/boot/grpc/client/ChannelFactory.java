package com.joeyorlando.boot.grpc.client;

import com.joeyorlando.boot.grpc.client.customizer.ChannelFactoryCustomizer;
import io.grpc.ClientInterceptor;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Component
public class ChannelFactory implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ChannelFactory.class);

    private List<ClientInterceptor> clientInterceptors;

    private List<ChannelFactoryCustomizer> customizers;

    private ClientProperties clientProperties;

    private List<Object> registeredServices = new ArrayList<>();

    private ManagedChannel managedChannel;

    private ManagedChannelBuilder builder;

    private String serverTarget;

    private boolean started = false;

    public ChannelFactory(@Autowired(required = false) ManagedChannelBuilder channelBuilder, ClientProperties clientProperties,
                          List<ClientInterceptor> clientInterceptors, List<ChannelFactoryCustomizer> customizers) {
        this.builder = channelBuilder;
        this.clientProperties = clientProperties;
        this.clientInterceptors = clientInterceptors;
        this.customizers = customizers;
        if (channelBuilder == null) {
            this.builder = forClientProperties(clientProperties);
        }
    }

    private void loadInterceptorBeans() {
        if (this.clientInterceptors != null) {
            for (ClientInterceptor interceptor : this.clientInterceptors) {
                LOG.info("Loading gRPC Interceptor Bean: {}", interceptor.getClass().getSimpleName());
                this.builder.intercept(interceptor);
            }
        }
    }

    public void registerInterceptor(ClientInterceptor clientInterceptor) {
        if (this.managedChannel != null) {
            throw new IllegalStateException("Cannot register Interceptor after ManagedChannel has been created");
        }
        this.clientInterceptors.add(clientInterceptor);
    }

    private void callCustomizerBeans(ManagedChannelBuilder channelBuilder) {
        LOG.debug("Invoking ChannelFactoryCustomizer beans unto the ManagedChannelFactory");
        if (customizers == null) {
            throw new IllegalStateException("Unable to invoke customizer beans for NULL ChannelFactoryCustomizer list");
        }

        if (channelBuilder == null) {
            throw new IllegalStateException("Unable to invoke customizer beans for NULL ManagedChannelBuilder");
        }

        for (ChannelFactoryCustomizer customizer : this.customizers) {
            LOG.info("Invoking gRPC Managed Channel customizer: {}", customizer.getClass().getName());
            customizer.customize(this, channelBuilder);
        }
    }

    @PostConstruct
    public void start() {
        callCustomizerBeans(this.builder);
        loadInterceptorBeans();
        this.started = true;
        if (this.builder == null) {
            throw new IllegalStateException("Cannot build NULL ManagedChannelBuilder");
        }
        LOG.debug("Initializing gRPC ManagedChannel");
        this.managedChannel = this.builder.build();
        LOG.info("gRPC ManagedChannel has been initialized");
    }

    public void shutdown() {
        if (this.managedChannel != null) {
            LOG.info("Shutting down gRPC ManagedChannel");
            this.started = false;
            this.managedChannel.shutdown();
        }
    }

    public ManagedChannel getManagedChannel() {
        return this.managedChannel;
    }

    private ManagedChannelBuilder forClientProperties(ClientProperties clientProperties) {
        if (clientProperties.getServerTarget() != null) {
            this.serverTarget = clientProperties.getServerTarget();
            return ManagedChannelBuilder.forTarget(clientProperties.getServerTarget());
        }
        if (clientProperties.getServerAddress() == null || clientProperties.getServerPort() == null) {
            throw new IllegalStateException("gRPC Client ServerAddress and ServerPort cannot be NULL");
        }
        this.serverTarget = clientProperties.getServerAddress() + ":" + clientProperties.getServerPort();
        return ManagedChannelBuilder.forAddress(clientProperties.getServerAddress(), clientProperties.getServerPort());
    }

    public <T> T registerService(Function<ManagedChannel, T> newStubCall) {
        if (managedChannel == null) {
            throw new IllegalStateException("Cannot registerService on NULL ManagedChannel");
        }
        LOG.info("Registering gRPC Service: {}", newStubCall.toString());
        T registeredService = newStubCall.apply(managedChannel);
        this.registeredServices.add(registeredService);
        return registeredService;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        boolean isDown = !this.started || this.managedChannel == null
                || this.managedChannel.isShutdown()
                || this.managedChannel.isTerminated();
        if (!isDown) {
            LOG.info("gRPC Managed Channel started for target: {}, with {} services",
                    Optional.ofNullable(this.serverTarget).orElse("UNAVAILABLE"),
                    this.registeredServices.size());
        } else {
            LOG.error("gRPC Managed Channel failed to start for target: {}", Optional.ofNullable(this.serverTarget).orElse("UNAVAILABLE"));
        }
    }
}
