package com.joeyorlando.boot.grpc.client.customizer;

import com.joeyorlando.boot.grpc.client.ClientProperties;
import com.joeyorlando.boot.grpc.client.ChannelFactory;
import com.joeyorlando.boot.grpc.client.util.ThreadUtil;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.instrument.async.LazyTraceExecutor;

import java.util.concurrent.Executor;

public class TracingChannelFactoryCustomizer extends ChannelFactoryCustomizer {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(InstrumentedChannelFactoryCustomizer.class);

    private Executor executor;

    @Autowired
    private BeanFactory beanFactory;

    public TracingChannelFactoryCustomizer(@Autowired(required = false) InstrumentedChannelFactoryCustomizer instrumentedCustomizer,
                                           ClientProperties clientProperties) {

        if (instrumentedCustomizer != null) {
            this.executor = instrumentedCustomizer.getExecutor();
        }
        if (this.executor == null) {
            LOG.info("Creating gRPC Managed Channel thread pool with minThreads: {}, maxThreads: {}",
                    clientProperties.getMinThreads(), clientProperties.getMaxThreads());
            this.executor = ThreadUtil.createExecutor(clientProperties);
        }
    }

    public TracingChannelFactoryCustomizer(ClientProperties clientProperties) {
        this(null, clientProperties);
    }

    @Override
    public void customize(ChannelFactory channelFactory, ManagedChannelBuilder channelBuilder) {
        channelBuilder.executor(new LazyTraceExecutor(beanFactory, this.executor));
    }
}
