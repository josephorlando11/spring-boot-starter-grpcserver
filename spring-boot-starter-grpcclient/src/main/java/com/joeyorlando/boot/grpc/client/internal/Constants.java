package com.joeyorlando.boot.grpc.client.internal;

import io.grpc.Metadata;

public final class Constants {
    public static final String METADATA_KEY_TRACINGID = "TracingId";
    public static final String METADATA_KEY_SPANID = "SpanId";

    public static final Metadata.Key<String> CLIENT_METADATA_TRACINGID = Metadata.Key.of(METADATA_KEY_TRACINGID, Metadata.ASCII_STRING_MARSHALLER);
    public static final Metadata.Key<String> CLIENT_METADATA_SPANID = Metadata.Key.of(METADATA_KEY_SPANID, Metadata.ASCII_STRING_MARSHALLER);

    private Constants() {
    }
}
