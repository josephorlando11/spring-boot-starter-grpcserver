package com.joeyorlando.boot.grpc.client.customizer;

import com.joeyorlando.boot.grpc.client.ClientProperties;
import com.joeyorlando.boot.grpc.client.ChannelFactory;
import com.joeyorlando.boot.grpc.client.util.ThreadUtil;
import io.grpc.ClientInterceptor;
import io.grpc.ManagedChannelBuilder;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.ToDoubleFunction;

public class InstrumentedChannelFactoryCustomizer extends ChannelFactoryCustomizer {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(InstrumentedChannelFactoryCustomizer.class);

    @Autowired(required = false)
    private List<ClientInterceptor> clientInterceptors = new ArrayList<>();

    @Autowired
    private MeterRegistry meterRegistry;

    @Autowired
    private ClientProperties clientProperties;

    private Executor threadPoolExecutor;
    private Gauge activeGauge;
    private Gauge coreGauge;
    private Gauge currentGauge;
    private Gauge maxGauge;

    public InstrumentedChannelFactoryCustomizer(@Autowired(required = false) @Qualifier("GrpcClientExecutor") ThreadPoolExecutor executor) {
        this.threadPoolExecutor = executor;
    }

    public InstrumentedChannelFactoryCustomizer() {
        this(null);
    }

    @PostConstruct
    public void start() {
        LOG.info("Creating gRPC Managed Channel thread pool with minThreads: {}, maxThreads: {}",
                clientProperties.getMinThreads(), clientProperties.getMaxThreads());
        ThreadPoolExecutor executor = ThreadUtil.createExecutor(clientProperties);

        LOG.debug("Instrumenting Managed Channel Executor");
        Iterable<Tag> meterTags = withMeterTags();
        this.activeGauge = configureGauge(clientProperties.getMetrics().getActiveThreadGauge(),
                meterTags, executor, ThreadPoolExecutor::getActiveCount);
        this.coreGauge = configureGauge(clientProperties.getMetrics().getCoreThreadGauge(),
                meterTags, executor, ThreadPoolExecutor::getCorePoolSize);
        this.currentGauge = configureGauge(clientProperties.getMetrics().getCurrentThreadGauge(),
                meterTags, executor, ThreadPoolExecutor::getPoolSize);
        this.maxGauge = configureGauge(clientProperties.getMetrics().getMaxThreadGauge(),
                meterTags, executor, ThreadPoolExecutor::getMaximumPoolSize);
        this.threadPoolExecutor = executor;
    }

    protected Iterable<Tag> withMeterTags() {
        return null;
    }

    private Gauge configureGauge(ClientProperties.Metrics.ThreadGaugeProperties threadGaugeProperties, Iterable<Tag> meterTags,
                                 ThreadPoolExecutor executor, ToDoubleFunction<ThreadPoolExecutor> func) {
        return Gauge.builder(threadGaugeProperties.getName(), executor, func)
                .tags(meterTags)
                .register(this.meterRegistry);
    }

    @Override
    public void customize(ChannelFactory channelFactory, ManagedChannelBuilder channelBuilder) {
        channelBuilder.executor(this.threadPoolExecutor);
    }

    public Executor getExecutor() {
        return this.threadPoolExecutor;
    }

    public void setExecutor(Executor executor) {
        this.threadPoolExecutor = executor;
    }
}
