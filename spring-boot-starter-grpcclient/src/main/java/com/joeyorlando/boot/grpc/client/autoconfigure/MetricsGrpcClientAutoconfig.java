package com.joeyorlando.boot.grpc.client.autoconfigure;

import com.joeyorlando.boot.grpc.client.customizer.InstrumentedChannelFactoryCustomizer;
import com.joeyorlando.boot.grpc.client.interceptors.InstrumentedClientInterceptor;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Auto-configuration for {@link InstrumentedClientInterceptor} to instrument a {@link io.micrometer.core.instrument.Timer},
 * bound to a {@link MeterRegistry}, for all outbound gRPC requests.
 */
@Configuration
@ConditionalOnBean({MeterRegistry.class})
@AutoConfigureBefore(GrpcClientAutoconfig.class)
public class MetricsGrpcClientAutoconfig {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(MetricsGrpcClientAutoconfig.class);

    @Bean
    public InstrumentedClientInterceptor metricsInterceptor() {
        LOG.info("Registering InstrumentedClientInterceptor");
        return new InstrumentedClientInterceptor();
    }

    @Bean
    public InstrumentedChannelFactoryCustomizer instrumentedGRpcCustomizer() {
        return new InstrumentedChannelFactoryCustomizer();
    }
}