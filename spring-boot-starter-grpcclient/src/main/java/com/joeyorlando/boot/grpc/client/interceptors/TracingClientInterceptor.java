package com.joeyorlando.boot.grpc.client.interceptors;

import brave.Tracer;
import brave.propagation.TraceContext;
import com.joeyorlando.boot.grpc.client.internal.Constants;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.ForwardingClientCall;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Client Interceptor to propagate current {@link Tracer} context to the server via request {@link Metadata}.
 */
public class TracingClientInterceptor implements ClientInterceptor {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(TracingClientInterceptor.class);

    @Autowired
    private Tracer tracer;

    @Override
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(final MethodDescriptor<ReqT, RespT> methodDescriptor, final CallOptions callOptions, final Channel channel) {
        return new DistributedTracingListener(channel.newCall(methodDescriptor, callOptions), methodDescriptor, tracer);
    }

    private class DistributedTracingListener<ReqT, RespT> extends ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT> {

        private final TraceContext currentContext;

        private final MethodDescriptor<ReqT, RespT> methodDescriptor;

        protected DistributedTracingListener(ClientCall delegate, MethodDescriptor methodDescriptor, Tracer currentTracer) {
            super(delegate);

            if (currentTracer == null) {
                this.currentContext = TraceContext.newBuilder().build();
            } else {
                this.currentContext = currentTracer.currentSpan().context();
            }

            if (methodDescriptor == null) {
                throw new IllegalStateException("MethodDescriptor cannot be NULL");
            }
            this.methodDescriptor = methodDescriptor;
        }

        @Override
        public void start(Listener<RespT> responseListener, Metadata headers) {
            LOG.debug("Applying TracingId: {}, SpanId: {} to outbound gRPC call: {}", currentContext.traceIdString(),
                    currentContext.spanIdString(), methodDescriptor.getFullMethodName());
            headers.put(Constants.CLIENT_METADATA_TRACINGID, String.valueOf(currentContext.traceId()));
            headers.put(Constants.CLIENT_METADATA_SPANID, String.valueOf(currentContext.spanId()));
            delegate().start(responseListener, headers);
        }
    }
}
