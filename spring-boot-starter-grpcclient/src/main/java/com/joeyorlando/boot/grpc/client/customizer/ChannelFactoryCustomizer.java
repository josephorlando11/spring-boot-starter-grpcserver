package com.joeyorlando.boot.grpc.client.customizer;

import com.joeyorlando.boot.grpc.client.ChannelFactory;
import io.grpc.ManagedChannelBuilder;

public abstract class ChannelFactoryCustomizer {
    public abstract void customize(ChannelFactory channelFactory, ManagedChannelBuilder channelBuilder);
}