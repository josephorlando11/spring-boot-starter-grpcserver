package com.joeyorlando.boot.grpc.client.interceptors;

import com.joeyorlando.boot.grpc.client.ClientProperties;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.ForwardingClientCall;
import io.grpc.ForwardingClientCallListener;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import io.grpc.Status;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

/**
 * Client Interceptor to implement Micrometer metrics.
 */
public class InstrumentedClientInterceptor implements ClientInterceptor {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(InstrumentedClientInterceptor.class);

    @Autowired
    private MeterRegistry meterRegistry;

    @Autowired
    private ClientProperties clientProperties;

    private Iterable<Tag> markedTags(String serviceName) {
        return Arrays.asList(Tag.of("service", serviceName));
    }

    @Override
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(final MethodDescriptor<ReqT, RespT> methodDescriptor, final CallOptions callOptions, final Channel channel) {
        LOG.debug("InstrumentedClientInterceptor is starting to record method: {}", methodDescriptor.getFullMethodName());
        Timer.Sample sample = Timer.start(meterRegistry);
        return new InstrumentedCall(channel.newCall(methodDescriptor, callOptions), this.meterRegistry, sample,
                clientProperties.getMetrics().getServiceDurationTimer().getName(), methodDescriptor);
    }

    private class InstrumentedCall<ReqT, RespT> extends ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT> {
        private final MeterRegistry meterRegistry;
        private final Timer.Sample sample;
        private final String timerName;
        private final MethodDescriptor methodDescriptor;

        protected InstrumentedCall(ClientCall delegate, MeterRegistry meterRegistry, Timer.Sample sample, String timerName, MethodDescriptor methodDescriptor) {
            super(delegate);
            if (meterRegistry == null) {
                throw new IllegalStateException("MeterRegistry cannot be NULL");
            }
            if (sample == null) {
                throw new IllegalStateException("Timer.Sample cannot be NULL");
            }
            if (timerName == null) {
                throw new IllegalStateException("TimerName cannot be NULL");
            }
            if (methodDescriptor == null) {
                throw new IllegalStateException("MethodDescriptor cannot be NULL");
            }
            this.meterRegistry = meterRegistry;
            this.sample = sample;
            this.timerName = timerName;
            this.methodDescriptor = methodDescriptor;
        }

        @Override
        public void start(final Listener<RespT> responseListener, final Metadata headers) {
            super.start(new InstrumentedListener<>(responseListener, this.meterRegistry, this.sample, this.timerName, this.methodDescriptor), headers);
        }
    }

    private class InstrumentedListener<RespT> extends ForwardingClientCallListener.SimpleForwardingClientCallListener<RespT> {
        private final MeterRegistry meterRegistry;
        private final Timer.Sample sample;
        private final String timerName;
        private final MethodDescriptor methodDescriptor;

        protected InstrumentedListener(ClientCall.Listener<RespT> delegate, MeterRegistry meterRegistry,
                                       Timer.Sample sample, String timerName, MethodDescriptor methodDescriptor) {
            super(delegate);
            this.meterRegistry = meterRegistry;
            this.sample = sample;
            this.timerName = timerName;
            this.methodDescriptor = methodDescriptor;
        }

        @Override
        public void onClose(final Status status, final Metadata trailers) {
            super.onClose(status, trailers);
            this.sample.stop(this.meterRegistry.timer(this.timerName, markedTags(this.methodDescriptor.getFullMethodName())));
            LOG.debug("InstrumentedClientInterceptor has finished recording method: {}", this.methodDescriptor.getFullMethodName());
        }
    }
}
