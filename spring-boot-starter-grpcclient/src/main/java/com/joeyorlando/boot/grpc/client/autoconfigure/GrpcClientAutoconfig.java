package com.joeyorlando.boot.grpc.client.autoconfigure;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.joeyorlando.boot.grpc.client")
public class GrpcClientAutoconfig {
}
