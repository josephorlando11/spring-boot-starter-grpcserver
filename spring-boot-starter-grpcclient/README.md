# Spring Boot gRPC Client
![pipeline](https://gitlab.com/josephorlando11/spring-boot-starter-grpcclient/badges/master/pipeline.svg) 
![coverage](https://gitlab.com/josephorlando11/spring-boot-starter-grpcclient/badges/master/coverage.svg)

A simple client implementation of gRPC for Spring Boot.  [Click Here for Java Documentation](https://josephorlando11.gitlab.io/spring-boot-starter-grpcclient/)

#### How to build

    $ ./gradlew clean build test
#### Dependencies
- Spring Boot Actuator (Micrometer)
    
    When paired with `spring-boot-starter-grpcclient` meters such as gauges and timers
    will be instrumented to monitor gRPC client thread pool and service call durations.
- Spring Cloud Sleuth (Brave/Zipkin)

    When paired with `spring-boot-starter-grpcclient` and `spring-boot-starter-grpcserver`, 
    interceptors will propagate `Tracer` and `Span` contexts from client to server, and 
    automatically initiate a new span for the server context. 

#### How to use

###### Example Spring Boot Application
```java
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
``` 

###### Example Gradle Dependencies
```groovy
dependencies {
    implementation ('org.springframework.boot:spring-boot-starter-actuator')
    implementation ('com.joeyorlando:spring-boot-starter-grpcclient:2.1.6.1.RELEASE')
    implementation ('org.springframework.cloud:spring-cloud-starter-sleuth:2.1.2.RELEASE')
}
``` 

#### Configuration Properties
The gRPC server beans can be configured using the following properties, their default values have been noted below:

```
# Port for gRPC server to bind to:
grpc.server.port=10000

# The minimum number of threads for the gRPC server's fixed thread pool:
grpc.server.minThreads=10

# The maximum number of threads for the gRPC server's fixed thread pool:
grpc.server.maxThreads=30

# The minimum number of threads for the gRPC server's fixed thread pool:
grpc.server.minThreads=10
```

The gRPC server instrumentation beans can be configured using the following properties, their default values have been noted below:
```
# Whether to enable the gRPC server active threads gauge:
grpc.server.metrics.activeThreadGauge.enabled=true

# Meter name for gRPC server active threads gauge:
grpc.server.metrics.activeThreadGauge.name="grpc.server.threads.active"

# Whether to enable the gRPC server core threads gauge:
grpc.server.metrics.coreThreadGauge.enabled=true

# Meter name for gRPC server core threads gauge:
grpc.server.metrics.coreThreadGauge.name="grpc.server.threads.active"

# Whether to enable the gRPC server current threads gauge:
grpc.server.metrics.currentThreadGauge.enabled=true

# Meter name for gRPC server current threads gauge:
grpc.server.metrics.currentThreadGauge.name="grpc.server.threads.current"

# Whether to enable the gRPC server max threads gauge:
grpc.server.metrics.maxThreadGauge.enabled=true

# Meter name for gRPC server max threads gauge:
grpc.server.metrics.maxThreadGauge.name="grpc.server.threads.max"

# Whether to enable the gRPC server call duration timer:
grpc.server.metrics.serviceDurationTimer.enabled=true

# Meter name for gRPC server call duration timer:
grpc.server.metrics.serviceDurationTimer.name="grpc.server.service.duration"

# Whether to enable the gRPC server call active gauge:
grpc.server.metrics.serviceActiveCalls.enabled=true

# Meter name for gRPC server call duration timer:
grpc.server.metrics.serviceActiveCalls.name="grpc.server.service.active"
```

    
#### Auto-configuration
Auto-configurations will be applied if `@EnableAutoConfiguration` is used (perhaps transitively via `@SpringBootApplication`) 
in the current Spring Boot context. Auto-configurations will implement the following components:

- DistributedTracingInterceptor

    Extracts and initializes `TraceContext` from all inbound requests.
 
- InstrumentedInterceptor

    Constructs `Timer` and `Gauge` and registers with `MeterRegistry` to monitor gRPC call execution.

- InstrumentedGrpcServerCustomizer
   
    Constructs a fixed thread pool for a gRPC calls, and instruments it with a `Gauge` for monitoring.
    