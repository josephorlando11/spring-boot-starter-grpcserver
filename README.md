# Spring Boot gRPC Server
![pipeline](https://gitlab.com/josephorlando11/spring-boot-starter-grpcserver/badges/master/pipeline.svg) 
![coverage](https://gitlab.com/josephorlando11/spring-boot-starter-grpcserver/badges/master/coverage.svg)

A simple server implementation of gRPC for Spring Boot. [Click Here for Java Documentation](https://josephorlando11.gitlab.io/spring-boot-starter-grpcserver/)

#### How to build

    $ ./gradlew clean build test
#### Dependencies
- Micrometer Metrics (Spring Boot Actuator)
    
    When paired with `spring-boot-starter-grpcserver` meters such as gauges and timers
    will be instrumented to monitor gRPC server thread pool and service call durations.

- Brave Distributed Tracing (Spring Cloud Sleuth)

    When paired with `spring-boot-starter-grpcserver` and `spring-boot-starter-grpcclient`, 
    interceptors will propagate `Tracer` context from client to server, and automatically 
    initiate a new span for the server's gRPC interceptor and call context. 

#### How to use

###### Example Spring Boot Application

A working example is included within the `spring-boot-starter-grpcserver-example` subproject.

###### Example Gradle Dependencies
```groovy
repositories {
    mavenCentral()
    maven {
        url 'https://gitlab.com/api/v4/packages/maven'
    }
}
    
dependencies {
    implementation ('org.springframework.boot:spring-boot-starter-actuator')
    implementation ('org.springframework.cloud:spring-cloud-starter-sleuth:2.1.2.RELEASE')
    implementation ('josephorlando11:spring-boot-starter-grpcserver:1.0.0')
}
``` 

#### Configuration Properties
The gRPC server beans can be configured using the following properties, their default values have been noted below:

```
# Port for gRPC server to bind to:
grpc.server.port=10000

# Whether to use the default gRPC Server executor, rather than a fixed thread pool.
# This must be false to enable metrics on the server thread pool.
grpc.server.defaultExecutor=false

# The minimum number of threads for the gRPC server's fixed thread pool:
grpc.server.minThreads=10

# The maximum number of threads for the gRPC server's fixed thread pool:
grpc.server.maxThreads=30
```

The gRPC server instrumentation beans can be configured using the following properties, their default values have been noted below:
```
# Whether to enable the gRPC server active threads gauge:
grpc.server.metrics.activeThreadGauge.enabled=true

# Meter name for gRPC server active threads gauge:
grpc.server.metrics.activeThreadGauge.name="grpc.server.threads.active"

# Whether to enable the gRPC server core threads gauge:
grpc.server.metrics.coreThreadGauge.enabled=true

# Meter name for gRPC server core threads gauge:
grpc.server.metrics.coreThreadGauge.name="grpc.server.threads.active"

# Whether to enable the gRPC server current threads gauge:
grpc.server.metrics.currentThreadGauge.enabled=true

# Meter name for gRPC server current threads gauge:
grpc.server.metrics.currentThreadGauge.name="grpc.server.threads.current"

# Whether to enable the gRPC server max threads gauge:
grpc.server.metrics.maxThreadGauge.enabled=true

# Meter name for gRPC server max threads gauge:
grpc.server.metrics.maxThreadGauge.name="grpc.server.threads.max"

# Whether to enable the gRPC server call duration timer:
grpc.server.metrics.serviceDurationTimer.enabled=true

# Meter name for gRPC server call duration timer:
grpc.server.metrics.serviceDurationTimer.name="grpc.server.service.duration"

# Whether to enable the gRPC server call active gauge:
grpc.server.metrics.serviceActiveCalls.enabled=true

# Meter name for gRPC server call duration timer:
grpc.server.metrics.serviceActiveCalls.name="grpc.server.service.active"
```

    
#### Auto-configuration
Auto-configurations will be applied if `@EnableAutoConfiguration` is used (perhaps transitively via `@SpringBootApplication`) 
in the current Spring Boot context. Auto-configurations will implement the following components:

- DistributedTracingInterceptor

    Extracts and initializes `TraceContext` from all inbound requests.
 
- InstrumentedInterceptor

    Constructs `Timer` and `Gauge` and registers with `MeterRegistry` to monitor gRPC call execution.

- InstrumentedGrpcServerCustomizer
   
    Constructs a fixed thread pool for a gRPC calls, and instruments it with a `Gauge` for monitoring.
    