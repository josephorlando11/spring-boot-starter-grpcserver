package com.joeyorlando.boot.grpc.server.customizer;

import com.joeyorlando.boot.grpc.server.GrpcServer;
import io.grpc.ServerBuilder;

/**
 * Customization for gRPC server features.
 */
public abstract class GrpcServerCustomizer {
    public abstract void customize(GrpcServer grpcServer, ServerBuilder serverBuilder);
}
