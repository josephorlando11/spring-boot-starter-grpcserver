package com.joeyorlando.boot.grpc.server.autoconfigure;

import com.joeyorlando.boot.grpc.server.customizer.InstrumentedGrpcServerCustomizer;
import com.joeyorlando.boot.grpc.server.interceptors.InstrumentedServerInterceptor;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Auto-configures an {@link InstrumentedServerInterceptor} in coordination with
 * a {@link MeterRegistry} bean.
 */
@Configuration
@ConditionalOnClass(MeterRegistry.class)
@AutoConfigureAfter(GrpcServerAutoconfig.class)
public class MetricsGrpcServerAutoconfig {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(MetricsGrpcServerAutoconfig.class);

    @Bean
    public InstrumentedServerInterceptor metricsGrpcServerInterceptor() {
        LOG.debug("Initializing InstrumentedServerInterceptor");
        return new InstrumentedServerInterceptor();
    }

    @Bean
    public InstrumentedGrpcServerCustomizer instrumentedGrpcServerCustomizer() {
        LOG.debug("Initializing InstrumentedGrpcServerCustomizer");
        return new InstrumentedGrpcServerCustomizer();
    }
}