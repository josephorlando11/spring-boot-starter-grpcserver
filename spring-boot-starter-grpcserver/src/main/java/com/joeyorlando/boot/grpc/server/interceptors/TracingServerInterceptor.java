package com.joeyorlando.boot.grpc.server.interceptors;

import brave.Span;
import brave.Tracer;
import brave.propagation.TraceContext;
import brave.propagation.TraceContextOrSamplingFlags;
import com.joeyorlando.boot.grpc.server.annotations.GrpcServerInterceptor;
import com.joeyorlando.boot.grpc.server.internal.Constants;
import com.joeyorlando.boot.grpc.server.ServerProperties;
import io.grpc.ForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * {@link ServerInterceptor} to extract {@link TraceContext} information from {@link Metadata} on
 * all inbound requests and initialize the context's next span for the gRPC call.
 */
@GrpcServerInterceptor(order = Ordered.HIGHEST_PRECEDENCE)
public final class TracingServerInterceptor implements ServerInterceptor {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(TracingServerInterceptor.class);

    @Autowired
    private Tracer tracer;

    @Autowired
    private ServerProperties serverProperties;

    @Autowired
    private Environment environment;

    private final String ipAddress;

    public TracingServerInterceptor() throws UnknownHostException {
        this.ipAddress = InetAddress.getLocalHost().getHostAddress();
    }

    private long findTraceId(Metadata headers, long orDefault) {
        try {
            return Long.parseLong(headers.get(Constants.CLIENT_METADATA_TRACINGID));
        } catch (Throwable throwable) {
            return orDefault;
        }
    }

    private long findSpanId(Metadata headers, long orDefault) {
        try {
            return Long.parseLong(headers.get(Constants.CLIENT_METADATA_SPANID));
        } catch (Throwable throwable) {
            return orDefault;
        }
    }

    private TraceContext getServerThreadContext() {
        if (this.tracer == null) {
            return TraceContext.newBuilder().build();
        } else if (this.tracer.currentSpan() == null) {
            return this.tracer.nextSpan().context();
        }
        return this.tracer.currentSpan().context();
    }

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(final ServerCall<ReqT, RespT> call,
                                                                 final Metadata headers, final ServerCallHandler<ReqT, RespT> next) {
        TraceContext serverThreadContext = getServerThreadContext();
        TraceContext clientContext = TraceContext.newBuilder()
                .traceId(findTraceId(headers, serverThreadContext.traceId()))
                .spanId(findSpanId(headers, serverThreadContext.spanId()))
                .parentId(serverThreadContext.traceId())
                .build();
        Span newSpan = tracer.nextSpan(TraceContextOrSamplingFlags.newBuilder()
                .context(clientContext)
                .build())
                .name("grpc.service/" + call.getMethodDescriptor().getFullMethodName())
                .kind(Span.Kind.SERVER);
        Optional<String> remoteServiceName = Optional.ofNullable(environment.getProperty("spring.application.name"));
        if (remoteServiceName.isPresent()) {
            newSpan.remoteServiceName(remoteServiceName.get());
        }
        newSpan.remoteIpAndPort(ipAddress, serverProperties.getPort());
        newSpan.start();
        try (Tracer.SpanInScope ws = tracer.withSpanInScope(newSpan)) {
            LOG.debug("Intercepting client call with serverThreadTraceId: {}, serverThreadSpanId: {}, " +
                            "clientTraceId: {}, clientSpanId: {}, currentSpanId: {}",
                    serverThreadContext.traceIdString(), serverThreadContext.spanIdString(),
                    clientContext.traceIdString(), clientContext.spanIdString(), newSpan.context().spanIdString());
            return new DistributedTracingInterceptorListener(tracer, newSpan, next.startCall(call, headers));
        } catch (RuntimeException | Error e) {
            if (newSpan != null) {
                newSpan.error(e);
                newSpan.finish();
            }
            throw e;
        }
    }

    private class DistributedTracingInterceptorListener<ReqT> extends ForwardingServerCallListener<ReqT> {
        private final Tracer tracer;
        private final Span span;
        private final ServerCall.Listener<ReqT> delegate;
        private final String traceId;
        private final String spanId;

        public DistributedTracingInterceptorListener(Tracer tracer, Span span, ServerCall.Listener<ReqT> delegate) {
            this.tracer = tracer;
            this.span = span;
            this.delegate = delegate;
            TraceContext context = this.span.context();
            this.traceId = context.traceIdString();
            this.spanId = context.spanIdString();
        }

        @Override
        protected ServerCall.Listener<ReqT> delegate() {
            return delegate;
        }

        @Override
        public void onMessage(ReqT message) {
            try (Tracer.SpanInScope ws = tracer.withSpanInScope(this.span)) {
                LOG.trace("Invoking gRPC onMessage delegate with traceId: {}, spanId: {}",
                        this.traceId, this.spanId);
                super.onMessage(message);
            }
        }

        @Override
        public void onHalfClose() {
            try (Tracer.SpanInScope ws = tracer.withSpanInScope(this.span)) {
                LOG.trace("Invoking gRPC onHalfClose delegate with traceId: {}, spanId: {}",
                        this.traceId, this.spanId);
                super.onHalfClose();
            }
        }

        @Override
        public void onCancel() {
            try (Tracer.SpanInScope ws = tracer.withSpanInScope(this.span)) {
                LOG.trace("Invoking gRPC onCancel delegate with traceId: {}, spanId: {}",
                        this.traceId, this.spanId);
                super.onCancel();
            }
        }

        @Override
        public void onComplete() {
            try (Tracer.SpanInScope ws = tracer.withSpanInScope(this.span)) {
                LOG.trace("Invoking gRPC onComplete delegate with traceId: {}, spanId: {}",
                        this.traceId, this.spanId);
                super.onComplete();
            } finally {
                if (this.span != null) {
                    LOG.trace("Closing TraceContext for traceId: {}, spanId: {}",
                            this.traceId, this.spanId);
                    this.span.finish();
                }
            }
        }
        @Override
        public void onReady() {
            try (Tracer.SpanInScope ws = tracer.withSpanInScope(this.span)) {
                LOG.trace("Invoking gRPC onReady delegate with traceId: {}, spanId: {}",
                        this.traceId, this.spanId);
                super.onReady();
            }
        }
    }
}