package com.joeyorlando.boot.grpc.server.autoconfigure;

import brave.Span;
import brave.Tracer;
import com.joeyorlando.boot.grpc.server.ServerProperties;
import com.joeyorlando.boot.grpc.server.customizer.InstrumentedGrpcServerCustomizer;
import com.joeyorlando.boot.grpc.server.customizer.TracingGrpcServerCustomizer;
import com.joeyorlando.boot.grpc.server.interceptors.TracingServerInterceptor;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.net.UnknownHostException;

/**
 * Auto-configures a {@link TracingServerInterceptor} in coordination with Brave
 * via Spring Boot Starter Sleuth.
 */
@Configuration
@ConditionalOnClass({Tracer.class, Span.class})
@AutoConfigureAfter({MetricsGrpcServerAutoconfig.class})
public class TracingGrpcServerAutoconfig {
    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(TracingGrpcServerAutoconfig.class);

    @Bean
    public TracingServerInterceptor tracingClientInterceptor() throws UnknownHostException {
        LOG.debug("Initializing TracingServerInterceptor");
        return new TracingServerInterceptor();
    }

    @Bean
    public TracingGrpcServerCustomizer tracingGrpcServerCustomizer(@Autowired(required = false) InstrumentedGrpcServerCustomizer instrumentedCustomizer,
                                                                             ServerProperties serverProperties) {
        LOG.debug("Initializing TracingGrpcServerCustomizer");
        return new TracingGrpcServerCustomizer(instrumentedCustomizer, serverProperties);
    }
}
