package com.joeyorlando.boot.grpc.server.annotations;

import org.springframework.core.Ordered;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates the order in which a particular {@link io.grpc.ServerInterceptor}
 * should be registered with the {@link io.grpc.Server}.
 *
 * {@link Ordered#HIGHEST_PRECEDENCE} will be the first interceptor to run.
 * {@link Ordered#LOWEST_PRECEDENCE} will be the first interceptor to run.
 *
 * Default interceptor order is: {@value DEFAULT_ORDER}
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface GrpcServerInterceptor {
    public static final int DEFAULT_ORDER = 0;
    public int order() default DEFAULT_ORDER;
    public String name() default "";
}
