package com.joeyorlando.boot.grpc.server;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * gRPC Server stub call responder assists with gRPC call responses by automatically generating gRPC call responses,
 * including authorized error information from a given {@link Throwable}, and ensures the response is {@code Completed}.
 *
 * When performing {@code Error} responses, the given {@link Throwable} information is populated into the following fields:
 * {@link Status#withDescription(String)} and {@link Status#withCause(Throwable)}, and can be configured to return either an
 * {@code Exception} or {@code RuntimeException}, with optional {@link Metadata} trailers.
 *
 * Exception information will only be exposed to the client for {@code AuthorizedExceptions}, which can be defined
 * by overriding the {@link #getAuthorizedPackages}, {@link #getAuthorizedPackageNames} and {@link #getAuthorizedClasses} methods.
 *
 * Any {@link Package} entries listed in {@link #getAuthorizedPackages} will be merged with any {@link Package} entries
 * in {@link #getAuthorizedPackageNames}.
 *
 * If {@link #shouldAuthorizeChildPackage} is enabled, all {@link Package} entries included in {@link #getAuthorizedPackages}
 * will be considered as a {@code basePackage} hierarchy, and any child packages will be considered as authorized.
 *
 * If {@link #shouldAuthorizeSubclass} is enabled, any subclasses of the listed {@link #getAuthorizedClasses} {@link Class} entries
 * will be considered as authorized.
 *
 * If {@link #getAuthorizedPackages} and {@link #getAuthorizedClasses} are both {@code NULL}, all {@link Throwable} objects
 * will be considered as authorized.
 *
 * Unauthorized Exception behavior can be controlled by overriding the {@link #onUnauthorized()} function. This function will be
 * called whenever a response is attempted using an unauthorized {@link Throwable}. {@link #onUnauthorized()} will be called on
 * the given unauthorized {@link Throwable}, and the resulting {@link Status} will be sent to the client.
 *
 * Customized failure behavior is supported. If an issue is encountered while responding, such as a {@link Throwable} being thrown
 * by the {@link io.grpc.Server}, the {@link #onFailure()} function will be called on the thrown {@link Throwable},
 * the resulting {@link Status} will be sent to the client, and the thrown {@link Throwable} will be propagated to the calling method.
 */
@Component
@ConditionalOnMissingBean(Responder.class)
public class Responder {
    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(Responder.class);

    protected String[] getAuthorizedPackageNames() {
        return null;
    }

    protected Package[] getAuthorizedPackages() {
        return null;
    }

    protected Class[] getAuthorizedClasses() {
        return null;
    }

    protected boolean shouldAuthorizeSubclass() {
        return true;
    }

    protected boolean shouldAuthorizeChildPackage() {
        return true;
    }

    protected Function<Throwable, Status> onUnauthorized() {
        return throwable -> {
            LOG.warn("Attempted to respond to gRPC call with unauthorized throwable of type: {}", exceptionName(throwable));
            Throwable unauthorizedException = new IllegalStateException("An unauthorized exception occurred: " + exceptionName(throwable));
            return Status.INTERNAL
                    .withDescription(unauthorizedException.getMessage())
                    .withCause(unauthorizedException);
        };
    }

    protected Function<Throwable, Status> onFailure() {
        return throwable -> {
            LOG.error("An unexpected error occurred while responding to gRPC call with Throwable", exceptionName(throwable));
            Throwable unexpectedException = new IllegalStateException("An unexpected exception occurred: " + exceptionName(throwable));
            return Status.INTERNAL
                    .withDescription(unexpectedException.getMessage())
                    .withCause(unexpectedException);
        };
    }

    public <T> void unaryError(StreamObserver<T> responseObserver, Throwable throwable, Status statusIn,
                               boolean runtimeException, Metadata trailers) {
        try {
            if (!isThrowableAuthorized(throwable)) {
                Status responseStatus = onUnauthorized().apply(throwable);
                LOG.trace("Responding to gRPC call with status: {} of type: {} due to unauthorized throwable of type: {}",
                        responseStatus, runtimeException ? "StatusRuntimeException" : "StatusException", exceptionName(throwable));
                doRespond(responseObserver, responseStatus, runtimeException, trailers);
            } else {
                Status responseStatus = statusIn.withDescription(throwable.getMessage()).withCause(throwable);
                LOG.trace("Responding to gRPC call with status: {} of type: {} mapped to throwable of type: {}",
                        responseStatus, runtimeException ? "StatusRuntimeException" : "StatusException", exceptionName(throwable));
                doRespond(responseObserver, responseStatus, runtimeException, trailers);
            }
        } catch (Throwable grpcException) {
            Status responseStatus = onFailure().apply(grpcException);
            LOG.trace("Responding to gRPC call with status: {} of type: {} due to unexpected exception of type: {}",
                    responseStatus, runtimeException ? "StatusRuntimeException" : "StatusException", exceptionName(grpcException));
            doRespond(responseObserver, responseStatus, runtimeException, trailers);
            throw grpcException;
        } finally {
            responseObserver.onCompleted();
        }
    }

    public <T> void unaryError(StreamObserver<T> responseObserver, Throwable throwableIn, Status statusIn) {
        unaryError(responseObserver, throwableIn, statusIn, false, null);
    }

    public <T> void unaryError(StreamObserver<T> responseObserver, Throwable throwableIn) {
        unaryError(responseObserver, throwableIn, Status.INTERNAL, false, null);
    }

    public <T> void unaryResponse(StreamObserver<T> responseObserver, T message) {
        if (responseObserver == null) {
            throw new IllegalStateException("ResponseObserver cannot be NULL");
        }
        try {
            responseObserver.onNext(message);
        } catch (Throwable throwable) {
            LOG.error("Unexpected exception occurred while writing to ResponseObserver", throwable);
            doRespond(responseObserver, onFailure().apply(throwable), true);
        } finally {
            responseObserver.onCompleted();
        }
    }

    public <T> StreamResponder<T> streamResponse(StreamObserver<T> responseObserver) {
        return new StreamResponder(this, responseObserver);
    }

    protected List<String> getAllAuthorizedPackages() {
        Package[] authorizedPackages = getAuthorizedPackages();
        String[] authorizedPackageNames = getAuthorizedPackageNames();
        if (authorizedPackages == null && authorizedPackageNames == null) {
            return null;
        }

        List<String> aggregatedPackages = new ArrayList<>();
        if (authorizedPackages != null) {
            for (Package authorizedPackage : authorizedPackages) {
                aggregatedPackages.add(authorizedPackage.getName().toLowerCase());
            }
        }
        if (authorizedPackageNames != null) {
            for (String authorizedPackageName : authorizedPackageNames) {
                aggregatedPackages.add(authorizedPackageName.toLowerCase());
            }
        }
        return aggregatedPackages;
    }

    boolean isThrowableAuthorized(Throwable throwable) {
        if (throwable == null) {
            throw new IllegalStateException("The Throwable given to an ExceptionResponder cannot be NULL");
        }

        Class throwableCls = throwable.getClass();
        Boolean trustedByPackage = null;
        List<String> authorizedPackages = getAllAuthorizedPackages();
        if (authorizedPackages != null) {
            trustedByPackage = false;
            for (String authorizedPackageStr : authorizedPackages) {
                String throwablePackageStr = throwableCls.getPackage().getName().toLowerCase();
                if (shouldAuthorizeChildPackage() && throwablePackageStr.startsWith(authorizedPackageStr)) {
                    trustedByPackage = true;
                } else if (throwablePackageStr.equals(authorizedPackageStr)) {
                    trustedByPackage = true;
                }
            }
        }

        Boolean trustedByClass = null;
        Class[] authorizedClasses = getAuthorizedClasses();
        if (authorizedClasses != null) {
            trustedByClass = false;
            for (Class authorizedClass : authorizedClasses) {
                if (shouldAuthorizeSubclass() && throwableCls.isAssignableFrom(authorizedClass)) {
                    trustedByClass = true;
                } else if (throwableCls.equals(authorizedClass)) {
                    trustedByClass = true;
                }
            }
        }

        if ((trustedByPackage != null || trustedByClass != null)
                && (trustedByPackage == null || !trustedByPackage.booleanValue())
                && (trustedByClass == null || !trustedByClass.booleanValue())) {
            return false;
        }
        return true;
    }

    static <T> void doRespond(StreamObserver<T> responseObserver, Throwable status,
                              boolean runtimeException, Metadata trailers) {
        Status responseStatus = Status.INTERNAL.withCause(status).withDescription(status == null ? null : status.getMessage());
        doRespond(responseObserver, responseStatus, runtimeException, trailers);
    }

    static <T> void doRespond(StreamObserver<T> responseObserver, Throwable status, boolean runtimeException) {
        doRespond(responseObserver, status, runtimeException, null);
    }

    static <T> void doRespond(StreamObserver<T> responseObserver, Throwable status) {
        doRespond(responseObserver, status, true, null);
    }

    static <T> void doRespond(StreamObserver<T> responseObserver, Status status,
                              boolean runtimeException, Metadata trailers) {
        if (runtimeException) {
            responseObserver.onError(status.asRuntimeException(trailers));
        } else {
            responseObserver.onError(status.asException(trailers));
        }
    }

    static <T> void doRespond(StreamObserver<T> responseObserver, Status status, boolean runtimeException) {
        doRespond(responseObserver, status, runtimeException, null);
    }

    static String exceptionName(Throwable throwable) {
        if (throwable == null) {
            return "NULL";
        }
        return throwable.getClass().getSimpleName() + ": " + throwable.getMessage();
    }

    public class StreamResponder<T> implements Closeable {
        private final Responder responder;
        private final StreamObserver<T> responseObserver;

        public StreamResponder(Responder responder, StreamObserver<T> responseObserver) {
            if (responseObserver == null) {
                throw new IllegalStateException("StreamResponder ResponseObserver cannot be NULL");
            }
            this.responder = responder;
            this.responseObserver = responseObserver;
        }

        public void onNext(T message) {
            try {
                responseObserver.onNext(message);
            } catch (Throwable throwable) {
                LOG.error("Unexpected exception occurred while writing to StreamResponder's ResponseObserver", throwable);
                doRespond(responseObserver, onFailure().apply(throwable), true);
            }
        }

        public void onError(Status status, boolean runtimeException, Metadata trailers) {
            Responder.doRespond(this.responseObserver, status, runtimeException, trailers);
        }

        public void onError(Status status, boolean runtimeException) {
            Responder.doRespond(this.responseObserver, status, runtimeException, null);
        }

        public void onError(Status status) {
            Responder.doRespond(this.responseObserver, status, true);
        }

        public void onError(Throwable throwable, boolean runtimeException, Metadata trailers) {
            if (!responder.isThrowableAuthorized(throwable)) {
                Responder.doRespond(this.responseObserver, responder.onUnauthorized().apply(throwable),
                        runtimeException, trailers);
            } else {
                Responder.doRespond(this.responseObserver, throwable, runtimeException, trailers);
            }
        }

        public void onError(Throwable throwable, boolean runtimeException) {
            this.onError(throwable, runtimeException, null);
        }

        public void onError(Throwable throwable) {
            this.onError(throwable, true);
        }

        public void onComplete() {
            this.responseObserver.onCompleted();
        }

        public void close() {
            this.onComplete();
        }
    }
}
