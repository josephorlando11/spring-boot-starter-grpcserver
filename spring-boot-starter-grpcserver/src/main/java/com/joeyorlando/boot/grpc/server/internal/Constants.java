package com.joeyorlando.boot.grpc.server.internal;

import com.joeyorlando.boot.grpc.server.ServerProperties;
import io.grpc.Metadata;

public final class Constants {
    public static final String DEFAULT_EXECUTOR_ENABLED = ServerProperties.PREFIX + "." + ServerProperties.PROPERTY_DEFAULT_EXECUTOR;

    public static final String METADATA_KEY_TRACINGID = "TracingId";
    public static final String METADATA_KEY_SPANID = "SpanId";

    public static final Metadata.Key<String> CLIENT_METADATA_TRACINGID = Metadata.Key.of(METADATA_KEY_TRACINGID, Metadata.ASCII_STRING_MARSHALLER);
    public static final Metadata.Key<String> CLIENT_METADATA_SPANID = Metadata.Key.of(METADATA_KEY_SPANID, Metadata.ASCII_STRING_MARSHALLER);

    private Constants() {
    }
}
