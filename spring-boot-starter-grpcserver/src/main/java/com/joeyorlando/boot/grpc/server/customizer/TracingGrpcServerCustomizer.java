package com.joeyorlando.boot.grpc.server.customizer;

import com.joeyorlando.boot.grpc.server.GrpcServer;
import com.joeyorlando.boot.grpc.server.ServerProperties;
import com.joeyorlando.boot.grpc.server.util.ThreadUtil;
import io.grpc.ServerBuilder;
import org.slf4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.sleuth.instrument.async.LazyTraceExecutor;

import java.util.concurrent.Executor;

public class TracingGrpcServerCustomizer extends GrpcServerCustomizer {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(TracingGrpcServerCustomizer.class);

    @Autowired
    private BeanFactory beanFactory;

    private Executor executor;

    public TracingGrpcServerCustomizer(InstrumentedGrpcServerCustomizer instrumentedCustomizer, ServerProperties serverProperties) {
        if (serverProperties.isDefaultExecutor()) {
            LOG.warn("{} bean was invoked but not applied due to {}.isDefaultExecutor: {}", this.getClass().getSimpleName(),
                    serverProperties.getClass().getSimpleName(), serverProperties.isDefaultExecutor());
        } else {
            if (instrumentedCustomizer != null) {
                this.executor = instrumentedCustomizer.getExecutor();
                LOG.debug("Binding tracing to executor retrieved from InstrumentedGrpcServerCustomizer: {}", this.executor.hashCode());
            }
            if (this.executor == null) {
                LOG.info("Creating gRPC Server thread pool with minThreads: {}, maxThreads: {}",
                        serverProperties.getMinThreads(), serverProperties.getMaxThreads());
                this.executor = ThreadUtil.createExecutor(serverProperties);
                LOG.debug("Binding tracing to executor: {}", this.executor.hashCode());
            }
        }
    }

    public TracingGrpcServerCustomizer(@Autowired(required = false) @Qualifier("GrpcServerExecutor") Executor executor, ServerProperties serverProperties) {
        if (serverProperties.isDefaultExecutor()) {
            LOG.warn("{} bean was invoked but not applied due to {}.isDefaultExecutor: {}", this.getClass().getSimpleName(),
                    serverProperties.getClass().getSimpleName(), serverProperties.isDefaultExecutor());
        } else {
            if (executor == null) {
                throw new IllegalStateException("Cannot bind tracing to NULL executor");
            }
            this.executor = executor;
            LOG.debug("Binding tracing to executor: {}", this.executor.hashCode());
        }
    }

    public TracingGrpcServerCustomizer(ServerProperties serverProperties) {
        this((InstrumentedGrpcServerCustomizer) null, serverProperties);
    }

    @Override
    public void customize(GrpcServer grpcServer, ServerBuilder serverBuilder) {
        if (this.executor != null) {
            serverBuilder.executor(new LazyTraceExecutor(beanFactory, executor));
        }
    }
}
