package com.joeyorlando.boot.grpc.server.autoconfigure;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.joeyorlando.boot.grpc.server")
public class GrpcServerAutoconfig {
}
