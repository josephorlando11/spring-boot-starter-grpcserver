package com.joeyorlando.boot.grpc.server.customizer;

import com.joeyorlando.boot.grpc.server.GrpcServer;
import com.joeyorlando.boot.grpc.server.util.ThreadUtil;
import io.grpc.ServerBuilder;
import com.joeyorlando.boot.grpc.server.ServerProperties;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * Provides the gRPC server with a {@link ThreadPoolExecutor} configured by the current context's
 * {@link ServerProperties#getMinThreads()} and {@link ServerProperties#getMaxThreads()}
 */
@Component
public class ThreadPoolGrpcServerCustomizer {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ThreadPoolGrpcServerCustomizer.class);

    private final ThreadPoolExecutor threadPoolExecutor;

    public ThreadPoolGrpcServerCustomizer(@Autowired(required = false) @Qualifier("GrpcServerExecutor") ThreadPoolExecutor executor,
                                          ServerProperties serverProperties) {
        if (serverProperties.isDefaultExecutor()) {
            LOG.warn("{} bean was invoked but not applied due to {}.isDefaultExecutor: {}", this.getClass().getSimpleName(),
                    serverProperties.getClass().getSimpleName(), serverProperties.isDefaultExecutor());
            executor = null;
        } else {
            if (executor == null) {
                executor = createExecutor(serverProperties);
            }
        }
        this.threadPoolExecutor = executor;
    }

    public void customize(GrpcServer grpcServer, ServerBuilder serverBuilder) {
        if (this.threadPoolExecutor != null) {
            serverBuilder.executor(this.threadPoolExecutor);
        }
    }

    private static ThreadPoolExecutor createExecutor(ServerProperties properties) {
        LOG.info("Creating gRPC server thread pool with minThreads: {}, maxThreads: {}",
                properties.getMinThreads(), properties.getMaxThreads());
        return ThreadUtil.createExecutor(properties);
    }
}
