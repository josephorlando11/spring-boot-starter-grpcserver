package com.joeyorlando.boot.grpc.server.annotations;

import com.joeyorlando.boot.grpc.server.GrpcServer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates a gRPC service bean for {@link GrpcServer} to register
 * as an available service.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface GrpcService {
}
