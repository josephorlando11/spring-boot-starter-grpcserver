package com.joeyorlando.boot.grpc.server.interceptors;

import com.joeyorlando.boot.grpc.server.ServerProperties;
import com.joeyorlando.boot.grpc.server.annotations.GrpcServerInterceptor;
import io.grpc.ForwardingServerCall;
import io.grpc.ForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.Status;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A {@link ServerInterceptor} which records incoming grpc calls to {@link Timer} and
 * {@link io.micrometer.core.instrument.Gauge}.
 */
@GrpcServerInterceptor
public final class InstrumentedServerInterceptor implements ServerInterceptor {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(InstrumentedServerInterceptor.class);

    protected static final String ENDPOINT_TAG_KEY = "endpoint";

    @Autowired
    private MeterRegistry meterRegistry;

    @Autowired
    private ServerProperties serverProperties;

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata requestMetadata, ServerCallHandler<ReqT, RespT> next) {
        return new InstrumentedListener<>(next, requestMetadata, call, this.meterRegistry, call.getMethodDescriptor());
    }

    private class InstrumentedListener<ReqT, RespT> extends ForwardingServerCallListener<ReqT> {

        private final ServerCall.Listener<ReqT> listenerDelegate;
        private final InstrumentedCall instrumentedServerCall;

        @SuppressWarnings("unchecked")
        InstrumentedListener(ServerCallHandler<ReqT, RespT> next, Metadata requestMetadata, ServerCall<ReqT, RespT> callDelegate,
                             MeterRegistry meterRegistry, MethodDescriptor methodDescriptor) {
            super();
            this.instrumentedServerCall = new InstrumentedCall(meterRegistry, callDelegate, methodDescriptor);
            this.listenerDelegate = next.startCall(this.instrumentedServerCall, requestMetadata);
        }

        @Override
        protected ServerCall.Listener<ReqT> delegate() {
            return listenerDelegate;
        }

        @Override
        public void onMessage(ReqT request) {
            super.onMessage(request);
        }

        @Override
        public void onReady() {
            this.instrumentedServerCall.onReady();
            super.onReady();
        }
    }

    private class InstrumentedCall<R, S> extends ForwardingServerCall.SimpleForwardingServerCall<R, S> {
        private final MethodDescriptor methodDescriptor;
        private final MeterRegistry meterRegistry;
        private final AtomicInteger busyGauge;
        private final Iterable<Tag> meterTags;
        private Timer.Sample timer;

        InstrumentedCall(MeterRegistry meterRegistry, ServerCall<R, S> delegate, MethodDescriptor methodDescriptor) {
            super(delegate);
            this.methodDescriptor = methodDescriptor;
            this.meterTags = createTags(methodDescriptor);
            this.meterRegistry = meterRegistry;
            this.busyGauge = meterRegistry.gauge(serverProperties.getMetrics().getServiceActiveCalls().getName(), meterTags, new AtomicInteger(0));
        }

        public void onReady() {
            recordStartMetric();
        }

        @Override
        public void close(Status status, Metadata responseHeaders) {
            recordEndMetric();
            super.close(status, responseHeaders);
        }

        @Override
        public void sendMessage(S message) {
            super.sendMessage(message);
        }

        protected Iterable<Tag> createTags(MethodDescriptor methodDescriptor) {
            return Arrays.asList(Tag.of(ENDPOINT_TAG_KEY, methodDescriptor.getFullMethodName()));
        }

        protected void recordStartMetric() {
            LOG.debug("Starting gRPC call timer on endpoint: {}", this.methodDescriptor.getFullMethodName());
            this.timer = Timer.start(meterRegistry);
            this.busyGauge.incrementAndGet();
        }

        protected void recordEndMetric() {
            this.timer.stop(meterRegistry.timer(serverProperties.getMetrics().getServiceDurationTimer().getName(), this.meterTags));
            this.busyGauge.decrementAndGet();
            LOG.debug("Stopped gRPC call timer on endpoint: {}", this.methodDescriptor.getFullMethodName());
        }
    }
}
