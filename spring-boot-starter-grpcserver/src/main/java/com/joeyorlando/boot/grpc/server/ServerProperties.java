package com.joeyorlando.boot.grpc.server;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * gRPC Server Properties
 */
@Configuration
@ConfigurationProperties(prefix = ServerProperties.PREFIX)
public class ServerProperties {
    public static final String PREFIX = "grpc.server";
    public static final String PROPERTY_DEFAULT_EXECUTOR = "defaultExecutor";

    private int port = 10000;
    private int minThreads = 10;
    private int maxThreads = 30;
    private boolean defaultExecutor = false;
    private Metrics metrics;

    public ServerProperties() {
        this.metrics = new Metrics();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getMinThreads() {
        return minThreads;
    }

    public void setMinThreads(int minThreads) {
        this.minThreads = minThreads;
    }

    public int getMaxThreads() {
        return maxThreads;
    }

    public void setMaxThreads(int maxThreads) {
        this.maxThreads = maxThreads;
    }

    public boolean isDefaultExecutor() {
        return defaultExecutor;
    }

    public void setDefaultExecutor(boolean defaultExecutor) {
        this.defaultExecutor = defaultExecutor;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public void setMetrics(Metrics metrics) {
        this.metrics = metrics;
    }

    public static class Metrics {
        private ActiveThreadGauge activeThreadGauge;
        private CoreThreadGauge coreThreadGauge;
        private CurrentThreadGauge currentThreadGauge;
        private MaxThreadGauge maxThreadGauge;
        private ServiceDurationTimer serviceDurationTimer;
        private ServiceActiveCalls serviceActiveCalls;

        public Metrics() {
            this.activeThreadGauge = new ActiveThreadGauge();
            this.coreThreadGauge = new CoreThreadGauge();
            this.currentThreadGauge = new CurrentThreadGauge();
            this.maxThreadGauge = new MaxThreadGauge();
            this.serviceDurationTimer = new ServiceDurationTimer();
            this.serviceActiveCalls = new ServiceActiveCalls();
        }

        public ActiveThreadGauge getActiveThreadGauge() {
            return activeThreadGauge;
        }

        public void setActiveThreadGauge(ActiveThreadGauge activeThreadGauge) {
            this.activeThreadGauge = activeThreadGauge;
        }

        public CoreThreadGauge getCoreThreadGauge() {
            return coreThreadGauge;
        }

        public void setCoreThreadGauge(CoreThreadGauge coreThreadGauge) {
            this.coreThreadGauge = coreThreadGauge;
        }

        public CurrentThreadGauge getCurrentThreadGauge() {
            return currentThreadGauge;
        }

        public void setCurrentThreadGauge(CurrentThreadGauge currentThreadGauge) {
            this.currentThreadGauge = currentThreadGauge;
        }

        public MaxThreadGauge getMaxThreadGauge() {
            return maxThreadGauge;
        }

        public void setMaxThreadGauge(MaxThreadGauge maxThreadGauge) {
            this.maxThreadGauge = maxThreadGauge;
        }

        public ServiceDurationTimer getServiceDurationTimer() {
            return serviceDurationTimer;
        }

        public void setServiceDurationTimer(ServiceDurationTimer serviceDurationTimer) {
            this.serviceDurationTimer = serviceDurationTimer;
        }

        public ServiceActiveCalls getServiceActiveCalls() {
            return serviceActiveCalls;
        }

        public void setServiceActiveCalls(ServiceActiveCalls serviceActiveCalls) {
            this.serviceActiveCalls = serviceActiveCalls;
        }

        public static class ServiceDurationTimer implements MetricProperties {
            private boolean enabled = true;
            private String name = "grpc.server.service.duration";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public static class ServiceActiveCalls implements MetricProperties {
            private boolean enabled = true;
            private String name = "grpc.server.service.active";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public static class ActiveThreadGauge implements ThreadGaugeProperties {
            private boolean enabled = true;
            private String name = "grpc.server.threads.active";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public static class CoreThreadGauge implements ThreadGaugeProperties {
            private boolean enabled = true;
            private String name = "grpc.server.threads.core";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public static class CurrentThreadGauge implements ThreadGaugeProperties {
            private boolean enabled = true;
            private String name = "grpc.server.threads.current";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public static class MaxThreadGauge implements ThreadGaugeProperties {
            private boolean enabled = true;
            private String name = "grpc.server.threads.max";
            public boolean isEnabled() {
                return enabled;
            }
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
        }

        public interface ThreadGaugeProperties extends MetricProperties {
        }

        public interface MetricProperties {
            boolean isEnabled();
            void setEnabled(boolean enabled);
            String getName();
            void setName(String name);
        }
    }

    public static String propertyName(String property) {
        try {
            if (property != null && ServerProperties.class.getDeclaredField(property) != null) {
                return PREFIX + "." + property;
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
