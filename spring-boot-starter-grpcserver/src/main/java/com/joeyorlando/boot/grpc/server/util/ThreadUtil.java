package com.joeyorlando.boot.grpc.server.util;

import com.joeyorlando.boot.grpc.server.ServerProperties;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class ThreadUtil {

    private ThreadUtil() {
    }

    public static ThreadPoolExecutor createExecutor(ServerProperties properties) {
        return new ThreadPoolExecutor(properties.getMinThreads(), properties.getMaxThreads(),
                0L, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
    }
}
