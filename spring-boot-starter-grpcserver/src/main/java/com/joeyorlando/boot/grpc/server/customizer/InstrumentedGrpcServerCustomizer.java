package com.joeyorlando.boot.grpc.server.customizer;

import com.joeyorlando.boot.grpc.server.GrpcServer;
import com.joeyorlando.boot.grpc.server.ServerProperties;
import com.joeyorlando.boot.grpc.server.util.ThreadUtil;
import io.grpc.ServerBuilder;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.ToDoubleFunction;

/**
 * Customizes the context's {@link GrpcServer} bean with Micrometer instrumentation.
 * Provides the gRPC server with a {@link ThreadPoolExecutor} that is instrumented with {@link Gauge}.
 */
public class InstrumentedGrpcServerCustomizer extends GrpcServerCustomizer {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(InstrumentedGrpcServerCustomizer.class);

    @Autowired
    private ServerProperties serverProperties;

    @Autowired
    private MeterRegistry meterRegistry;

    private ThreadPoolExecutor threadPoolExecutor;
    private Gauge activeGauge;
    private Gauge currentGauge;
    private Gauge coreGauge;
    private Gauge maxGauge;

    public InstrumentedGrpcServerCustomizer(@Autowired(required = false) @Qualifier("GrpcServerExecutor") ThreadPoolExecutor executor) {
        this.threadPoolExecutor = executor;
    }

    public InstrumentedGrpcServerCustomizer() {
        this(null);
    }

    @PostConstruct
    public void buildInstrumentedExecutor() {
        if (serverProperties.isDefaultExecutor()) {
            LOG.warn("{} bean was invoked but not applied due to {}.isDefaultExecutor: {}", this.getClass().getSimpleName(),
                    serverProperties.getClass().getSimpleName(), serverProperties.isDefaultExecutor());
        } else {
            if (this.threadPoolExecutor == null) {
                this.threadPoolExecutor = createExecutor(serverProperties);
            }
            LOG.debug("Binding metrics to executor: {}", this.threadPoolExecutor.hashCode());
            Iterable<Tag> meterTags = withMeterTags();
            if (serverProperties.getMetrics().getActiveThreadGauge().isEnabled()) {
                this.activeGauge = configureGauge(serverProperties.getMetrics().getActiveThreadGauge(),
                        meterTags, this.threadPoolExecutor, ThreadPoolExecutor::getActiveCount);
            }

            if (serverProperties.getMetrics().getCoreThreadGauge().isEnabled()) {
                this.currentGauge = configureGauge(serverProperties.getMetrics().getCoreThreadGauge(),
                        meterTags, this.threadPoolExecutor, ThreadPoolExecutor::getCorePoolSize);
            }

            if (serverProperties.getMetrics().getCurrentThreadGauge().isEnabled()) {
                this.currentGauge = configureGauge(serverProperties.getMetrics().getCurrentThreadGauge(),
                        meterTags, this.threadPoolExecutor, ThreadPoolExecutor::getPoolSize);
            }

            if (serverProperties.getMetrics().getMaxThreadGauge().isEnabled()) {
                this.currentGauge = configureGauge(serverProperties.getMetrics().getMaxThreadGauge(),
                        meterTags, this.threadPoolExecutor, ThreadPoolExecutor::getMaximumPoolSize);
            }
        }
    }

    @Override
    public void customize(GrpcServer grpcServer, ServerBuilder serverBuilder) {
        if (this.threadPoolExecutor != null) {
            serverBuilder.executor(this.threadPoolExecutor);
        }
    }

    public Executor getExecutor() {
        return this.threadPoolExecutor;
    }

    protected Iterable<Tag> withMeterTags() {
        return null;
    }

    private Gauge configureGauge(ServerProperties.Metrics.ThreadGaugeProperties threadGaugeProperties, Iterable<Tag> meterTags,
                                 ThreadPoolExecutor executor, ToDoubleFunction<ThreadPoolExecutor> func) {
        return Gauge.builder(threadGaugeProperties.getName(), executor, func)
                .tags(meterTags)
                .register(meterRegistry);
    }

    private static ThreadPoolExecutor createExecutor(ServerProperties properties) {
        LOG.info("Creating instrumented gRPC server thread pool with minThreads: {}, maxThreads: {}",
                properties.getMinThreads(), properties.getMaxThreads());
        return ThreadUtil.createExecutor(properties);
    }
}
