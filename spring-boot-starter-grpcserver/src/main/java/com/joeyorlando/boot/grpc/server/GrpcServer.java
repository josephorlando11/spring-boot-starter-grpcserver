package com.joeyorlando.boot.grpc.server;

import com.joeyorlando.boot.grpc.server.annotations.GrpcServerInterceptor;
import com.joeyorlando.boot.grpc.server.annotations.GrpcService;
import com.joeyorlando.boot.grpc.server.customizer.GrpcServerCustomizer;
import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerInterceptor;
import io.grpc.util.TransmitStatusRuntimeExceptionInterceptor;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Constructs a gRPC {@link Server} once all beans have initialized. Automatically finds
 * and registers all {@link ServerInterceptor} beans with the gRPC server, and invokes
 * all {@link GrpcServerCustomizer} customization beans onto itself.
 */
@Component
public class GrpcServer implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(GrpcServer.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ServerProperties serverProperties;

    @Autowired(required = false)
    private List<GrpcServerCustomizer> customizers = new ArrayList<>();

    private List<RegisteredInterceptor> registeredInterceptors = new ArrayList<>();

    private Map<String, BindableService> registeredServices = new HashMap<>();

    private Server server;

    private boolean started = false;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        discoverServiceBeans();
        LOG.debug("Initializing Grpc Server on port {} with {} registered services.", serverProperties.getPort(), registeredServices.size());
        ServerBuilder builder = ServerBuilder.forPort(serverProperties.getPort())
                .intercept(TransmitStatusRuntimeExceptionInterceptor.instance());
        callCustomizerBeans(builder);
        discoverInterceptorBeans();
        registerInterceptors(builder);
        registerServices(builder);
        this.server = builder.build();
        try {
            this.server.start();
            this.started = true;
        } catch (IOException e) {
            LOG.error("Unable to start gRPC server", e);
        }
        LOG.info("Grpc Server has been started on port {} with {} registered services.", serverProperties.getPort(), registeredServices.size());
    }

    /**
     * Discover all registered {@link GrpcService} beans and load them for registration.
     */
    private void discoverServiceBeans() {
        LOG.debug("Searching for all gRPC server service beans");
        for (Map.Entry<String, Object> serviceEntry : applicationContext.getBeansWithAnnotation(GrpcService.class).entrySet()) {
            this.registeredServices.put(serviceEntry.getKey(), (BindableService) serviceEntry.getValue());
        }
    }

    /**
     * Discover all registered {@link GrpcServerInterceptor} beans and load them for registration.
     */
    private void discoverInterceptorBeans() {
        Map<String, Object> serverInterceptors = applicationContext.getBeansWithAnnotation(GrpcServerInterceptor.class);
        serverInterceptors.forEach((name, interceptor) -> {
            // Verify the given bean is a valid gRPC ServerInterceptor
            verifyInterceptorBean(interceptor);
            GrpcServerInterceptor interceptorAnnotation = interceptor.getClass().getAnnotation(GrpcServerInterceptor.class);
            if (interceptorAnnotation == null) {
                throw new IllegalStateException("Unable to find GrpcServerInterceptor annotation for bean: "
                        + interceptor.getClass().getName());
            }

            // Assume the custom name (if any was given), otherwise default to bean name.
            name = (StringUtils.isEmpty(interceptorAnnotation.name())) ? name : interceptorAnnotation.name();
            LOG.trace("ServerInterceptor: {} was noted as having order: {}", interceptor.getClass().getSimpleName(),
                    interceptorAnnotation.order());
            this.registeredInterceptors.add(new RegisteredInterceptor((ServerInterceptor) interceptor, interceptorAnnotation.order(), name));
        });
    }

    private void verifyInterceptorBean(Object interceptor) {
        if (interceptor == null) {
            throw new IllegalStateException("Cannot register NULL GrpcServerInterceptor bean");
        }

        Class interceptorClass = interceptor.getClass();
        if (!ServerInterceptor.class.isAssignableFrom(interceptorClass)) {
            throw new IllegalStateException("Cannot register GrpcServerInterceptor bean of type: "
                    + interceptorClass.getSimpleName() + ". Must be of type: " + ServerInterceptor.class.getSimpleName());
        }
    }

    private void callCustomizerBeans(ServerBuilder serverBuilder) {
        LOG.trace("Applying GrpcServerCustomizer beans unto the gRPC server");
        if (customizers == null) {
            throw new IllegalStateException("Unable to invoke customizer beans for NULL GrpcServerCustomizer list");
        }

        if (serverBuilder == null) {
            throw new IllegalStateException("Unable to invoke customizer beans for NULL ServerBuilder");
        }

        for (GrpcServerCustomizer customizer : this.customizers) {
            LOG.debug("Applying gRPC server customizer: {}", customizer.getClass().getSimpleName());
            customizer.customize(this, serverBuilder);
        }
    }

    private void registerInterceptors(ServerBuilder serverBuilder) {
        LOG.debug("Registering interceptor beans for gRPC server");
        if (serverBuilder == null) {
            throw new IllegalStateException("Unable to register interceptors for NULL ServerBuilder");
        }

        this.registeredInterceptors.sort(new RegisteredInterceptorComparator());
        for (RegisteredInterceptor interceptor : this.registeredInterceptors) {
            LOG.info("Registering gRPC server Interceptor: {}", interceptor.getName());
            serverBuilder.intercept(interceptor.getInterceptor());
        }
    }

    private void registerServices(ServerBuilder serverBuilder) {
        LOG.debug("Registering service beans for gRPC server");
        if (registeredServices == null) {
            throw new IllegalStateException("Unable to register service beans for NULL BindableService list");
        }

        if (serverBuilder == null) {
            throw new IllegalStateException("Unable to register service beans for NULL ServerBuilder");
        }

        // Register all scanned gRPC services
        for (Map.Entry<String, BindableService> service : registeredServices.entrySet()) {
            String serviceName = Optional.ofNullable(service.getKey())
                    .orElse(service.getValue().getClass().getSimpleName());
            try {
                LOG.info("Registering GRPC service: {}", serviceName);
                serverBuilder.addService(service.getValue());
            } catch (Exception e) {
                LOG.error("An unhandled exception occurred while attempting to register service: " + serviceName, e);
            }
        }
    }

    public void registerService(BindableService service) {
        if (service != null) {
            if (this.started) {
                throw new IllegalStateException("Cannot register BindableService after GrpcServer has started");
            }
            this.registeredServices.put(service.getClass().getName(), service);
        }
    }

    public void registerInterceptor(ServerInterceptor service, int order, String name) {
        if (service != null) {
            if (this.started) {
                throw new IllegalStateException("Cannot register ServerInterceptor after GrpcServer has started");
            }
            this.registeredInterceptors.add(new RegisteredInterceptor(service, order, name));
        }
    }

    public void registerInterceptor(ServerInterceptor service, int order) {
        this.registerInterceptor(service, order, null);
    }

    public void registerInterceptor(ServerInterceptor service) {
        this.registerInterceptor(service, GrpcServerInterceptor.DEFAULT_ORDER);
    }

    public void shutdown() {
        if (server != null) {
            this.started = false;
            LOG.info("Shutting down gRPC server on port: {}", serverProperties.getPort());
            this.server.shutdown();
        }
    }

    public Server getServer() {
        return this.server;
    }

    public class RegisteredInterceptor implements Ordered {
        private final ServerInterceptor interceptor;
        private final int order;
        private final String name;

        private RegisteredInterceptor(ServerInterceptor interceptor, int order, String name) {
            if (interceptor == null) {
                throw new IllegalStateException("Cannot register a NULL ServerInterceptor");
            }
            this.interceptor = interceptor;
            this.order = order;
            this.name = Optional.ofNullable(name).orElse(interceptor.getClass().getName());
        }

        public ServerInterceptor getInterceptor() {
            return interceptor;
        }

        public String getName() {
            return this.name;
        }

        @Override
        public int getOrder() {
            return order;
        }
    }

    private class RegisteredInterceptorComparator implements Comparator<RegisteredInterceptor> {
        @Override
        public int compare(RegisteredInterceptor x, RegisteredInterceptor y) {
            return (x.getOrder() > y.getOrder()) ? -1 : 1;
        }
    }
}
