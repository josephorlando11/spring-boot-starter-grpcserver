package com.joeyorlando.boot.grpc.example.server.responders;

import com.joeyorlando.boot.grpc.server.Responder;
import org.springframework.stereotype.Component;

/**
 * Example {@link Responder} implementation.
 *
 * Authorizes all {@link Throwable} objects in the current {@link Package} to be used
 * for gRPC call responses.
 *
 * Also authorizes {@link IllegalArgumentException} to be used for gRPC call responses.
 */
@Component
public class CustomResponder extends Responder {

    @Override
    protected String[] getAuthorizedPackageNames() {
        return null;
    }

    // Authorize all Throwable objects in the current Package.
    @Override
    protected Package[] getAuthorizedPackages() {
        return new Package[] { this.getClass().getPackage() };
    }

    @Override
    protected Class[] getAuthorizedClasses() {
        return new Class[] { IllegalArgumentException.class };
    }

    @Override
    protected boolean shouldAuthorizeSubclass() {
        return true;
    }

    @Override
    protected boolean shouldAuthorizeChildPackage() {
        return true;
    }

}
