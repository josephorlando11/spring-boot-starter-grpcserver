package com.joeyorlando.boot.grpc.example.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

       /* @PostConstruct
    public void start() {
        Thread serviceThread = new Thread(new Runnable() {
            private final Logger LOG = org.slf4j.LoggerFactory.getLogger("ServiceThread");
            @Override
            public void run() {
                while (true) {
                    LOG.debug("Keepalive");
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, "");
        serviceThread.start();
    }*/
}