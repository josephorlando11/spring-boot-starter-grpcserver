package com.joeyorlando.boot.grpc.example.server.responders;

/**
 * An example exception that is authorized to be sent to the gRPC client
 * via {@link com.joeyorlando.boot.grpc.example.server.responders.CustomExceptionResponder}.
 */
public class GrpcServerException extends RuntimeException {
    public GrpcServerException(String message) {
        super(message);
    }
}
