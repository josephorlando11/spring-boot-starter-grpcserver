package com.joeyorlando.boot.grpc.example.server.interceptors;

import brave.Tracer;
import io.grpc.ForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExampleCompleteInterceptor implements ServerInterceptor {

    private final Logger LOG = org.slf4j.LoggerFactory.getLogger(ExampleCompleteInterceptor.class);

    @Autowired
    private Tracer tracer;

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        LOG.info("Example Complete Interceptor has fired with traceId: {}, spanId: {}", tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        ServerCall.Listener<ReqT> delegate = next.startCall(call, headers);
        return new ForwardingServerCallListener<ReqT>() {
            @Override
            protected ServerCall.Listener<ReqT> delegate() {
                return delegate;
            }
            @Override
            public void onComplete() {
                LOG.info("Example Complete Interceptor has fired ON COMPLETE with traceId: {}, spanId: {}", tracer.currentSpan().context().traceIdString(),
                        tracer.currentSpan().context().spanIdString());
                super.onComplete();
            }
        };
    }
}
