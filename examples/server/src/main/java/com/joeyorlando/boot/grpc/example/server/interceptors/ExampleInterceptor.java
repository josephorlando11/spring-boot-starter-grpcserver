package com.joeyorlando.boot.grpc.example.server.interceptors;

import brave.Tracer;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExampleInterceptor implements ServerInterceptor {

    private final Logger LOG = org.slf4j.LoggerFactory.getLogger(ExampleInterceptor.class);

    @Autowired
    private Tracer tracer;

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        LOG.info("Example Interceptor has fired with traceId: {}, spanId: {}", tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        return next.startCall(call, headers);
    }
}
