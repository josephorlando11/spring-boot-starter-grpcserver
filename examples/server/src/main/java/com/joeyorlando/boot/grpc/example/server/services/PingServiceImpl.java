package com.joeyorlando.boot.grpc.example.server.services;

import brave.Tracer;
import com.joeyorlando.boot.grpc.example.server.responders.GrpcServerException;
import com.joeyorlando.boot.grpc.server.Responder;
import com.joeyorlando.boot.grpc.server.annotations.GrpcService;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@GrpcService
@Service
public class PingServiceImpl extends PingGrpc.PingImplBase {

    private final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(PingServiceImpl.class);

    @Autowired
    private Tracer tracer;

    @Autowired
    private Responder responder;

    @Override
    public void unaryPing(PingMessage request, StreamObserver<PingMessage> responseObserver) {
        LOG.info("PING: {} with traceId: {}, spanId: {}", request.getMessage(), tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        responseObserver.onNext(PingMessage.newBuilder().setMessage("PONG").build());
        responseObserver.onCompleted();
    }

    @Override
    public void unaryPingWithResponder(PingMessage request, StreamObserver<PingMessage> responseObserver) {
        LOG.info("PING: {} with traceId: {}, spanId: {}", request.getMessage(), tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        responder.unaryResponse(responseObserver, PingMessage.newBuilder().setMessage("PONG").build());
    }

    @Override
    public void unaryPingWithPackageAuthorizedException(PingMessage request, StreamObserver<PingMessage> responseObserver) {
        LOG.info("PING: {} with traceId: {}, spanId: {}", request.getMessage(), tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        responder.unaryError(responseObserver, new GrpcServerException("A GrpcServerException has been thrown!"));
    }

    @Override
    public void unaryPingWithClassAuthorizedException(PingMessage request, StreamObserver<PingMessage> responseObserver) {
        LOG.info("PING: {} with traceId: {}, spanId: {}", request.getMessage(), tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        responder.unaryError(responseObserver, new IllegalArgumentException("An IllegalArgumentException has been thrown!"));
    }

    @Override
    public void unaryPingWithUnauthorizedException(PingMessage request, StreamObserver<PingMessage> responseObserver) {
        LOG.info("PING: {} with traceId: {}, spanId: {}", request.getMessage(), tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        responder.unaryError(responseObserver, new RuntimeException("A RuntimeException has been thrown!"));
    }




    @Override
    public void streamPing(PingMessage request, StreamObserver<PingMessage> responseObserver) {
        LOG.info("PING: {} with traceId: {}, spanId: {}", request.getMessage(), tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        responseObserver.onNext(PingMessage.newBuilder().setMessage("PONG").build());
        responseObserver.onCompleted();
    }

    @Override
    public void streamPingWithResponder(PingMessage request, StreamObserver<PingMessage> responseObserver) {
        LOG.info("PING: {} with traceId: {}, spanId: {}", request.getMessage(), tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        try (Responder.StreamResponder streamResponder = responder.streamResponse(responseObserver)) {
            for (int i = 0; i < 3; i++) {
                streamResponder.onNext(PingMessage.newBuilder().setMessage("PONG").build());
            }
        }
    }

    @Override
    public void streamPingWithPackageAuthorizedException(PingMessage request, StreamObserver<PingMessage> responseObserver) {
        LOG.info("PING: {} with traceId: {}, spanId: {}", request.getMessage(), tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        try (Responder.StreamResponder streamResponder = responder.streamResponse(responseObserver)) {
            streamResponder.onError(new GrpcServerException("An GrpcServerException has been thrown!"));
        }
    }

    @Override
    public void streamPingWithClassAuthorizedException(PingMessage request, StreamObserver<PingMessage> responseObserver) {
        LOG.info("PING: {} with traceId: {}, spanId: {}", request.getMessage(), tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        try (Responder.StreamResponder streamResponder = responder.streamResponse(responseObserver)) {
            streamResponder.onError(new IllegalArgumentException("An IllegalArgumentException has been thrown!"));
        }
    }

    @Override
    public void streamPingWithUnauthorizedException(PingMessage request, StreamObserver<PingMessage> responseObserver) {
        LOG.info("PING: {} with traceId: {}, spanId: {}", request.getMessage(), tracer.currentSpan().context().traceIdString(),
                tracer.currentSpan().context().spanIdString());
        try (Responder.StreamResponder streamResponder = responder.streamResponse(responseObserver)) {
            streamResponder.onError(new RuntimeException("A RuntimeException has been thrown!"));
        }
    }
}