# Example Spring Boot gRPC Server

A simple client implementation of gRPC for Spring Boot.

#### How to build

    $ ./gradlew clean build test

#### How to run

    $ java - jar build/libs/spring-boot-starter-grpcserver-example-2.1.6.1.RELEASE.jar

#### How to use

This project integrates with spring-boot-starter-grpcclient-example

###### What's included
- SimpleMeterRegistry to provide an example `MeterRegistry` bean for metrics instrumentation.
- Sampler for Brave distributed tracing implementation.
- ExampleInterceptor (ServerInterceptor) to provide an example ServerInterceptor
- ExampleCompleteInterceptor (ServerInterceptor) to provide an example ServerInterceptor with onComplete functionality.
- PingServiceImpl (Precompiled protobuf definitions) Example gRPC Service that provides a simple Ping/Pong interface.

#### Example Output

```
[/] DEBUG [main] 40979 --- [           main] c.j.b.g.s.a.MetricsGrpcServerAutoconfig  : Initializing InstrumentedInterceptor
[/] DEBUG [main] 40979 --- [           main] a.DistributedTracingGrpcServerAutoconfig : Initializing DistributedTracingInterceptor
[/] DEBUG [main] 40979 --- [           main] c.j.b.g.s.a.MetricsGrpcServerAutoconfig  : Initializing InstrumentedGrpcServerCustomizer
[/] INFO  [main] 40979 --- [           main] b.g.s.c.InstrumentedGrpcServerCustomizer : Creating gRPC server thread pool with minThreads: 10, maxThreads: 30
[/] DEBUG [main] 40979 --- [           main] b.g.s.c.InstrumentedGrpcServerCustomizer : Binding metrics to executor: 1637061418
[/] DEBUG [main] 40979 --- [           main] a.DistributedTracingGrpcServerAutoconfig : Initializing TracingGrpcServerCustomizer
[/] DEBUG [main] 40979 --- [           main] c.j.b.g.s.c.TracingGrpcServerCustomizer  : Binding tracing to executor retrieved from InstrumentedGrpcServerCustomizer: 1637061418
[/] INFO  [main] 40979 --- [           main] c.j.boot.grpc.server.Application         : Started Application in 2.241 seconds (JVM running for 2.743)
[/] DEBUG [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Searching for all gRPC server service beans
[/] DEBUG [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Initializing Grpc Server on port 10000 with 1 registered services.
[/] DEBUG [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Invoking GrpcServerCustomizer beans unto the gRPC server
[/] DEBUG [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Invoking gRPC server customizer: InstrumentedGrpcServerCustomizer
[/] DEBUG [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Invoking gRPC server customizer: TracingGrpcServerCustomizer
[/] DEBUG [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : ServerInterceptor: DistributedTracingInterceptor was noted as having order: 0
[/] DEBUG [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Registering interceptor beans for gRPC server
[/] INFO  [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Registering gRPC server Interceptor: exampleCompleteInterceptor
[/] INFO  [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Registering gRPC server Interceptor: exampleInterceptor
[/] INFO  [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Registering gRPC server Interceptor: metricsGrpcServerInterceptor
[/] INFO  [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Registering gRPC server Interceptor: tracingClientInterceptor
[/] DEBUG [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Registering service beans for gRPC server
[/] INFO  [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Registering GRPC service: pingServiceImpl
[/] INFO  [main] 40979 --- [           main] c.j.boot.grpc.server.GrpcServer          : Grpc Server has been started on port 10000 with 1 registered services.
[1fa6a748f5907a1e/59a723a9f03a0017] DEBUG [pool-1-thread-1] 40979 --- [pool-1-thread-1] .j.b.g.s.i.DistributedTracingInterceptor : Intercepting client call with serverThreadTraceId: 2ac02df81595e420, serverThreadSpanId: 2ac02df81595e420, clientTraceId: 1fa6a748f5907a1e, clientSpanId: 1fa6a748f5907a1e, currentSpanId: 59a723a9f03a0017
[1fa6a748f5907a1e/59a723a9f03a0017] INFO  [pool-1-thread-1] 40979 --- [pool-1-thread-1] c.j.b.g.s.i.ExampleInterceptor           : Example Interceptor has fired with traceId: 1fa6a748f5907a1e, spanId: 59a723a9f03a0017
[1fa6a748f5907a1e/59a723a9f03a0017] INFO  [pool-1-thread-1] 40979 --- [pool-1-thread-1] c.j.b.g.s.i.ExampleCompleteInterceptor   : Example Complete Interceptor has fired with traceId: 1fa6a748f5907a1e, spanId: 59a723a9f03a0017
[1fa6a748f5907a1e/59a723a9f03a0017] TRACE [pool-1-thread-1] 40979 --- [pool-1-thread-1] .j.b.g.s.i.DistributedTracingInterceptor : Invoking gRPC onReady delegate with traceId: 1fa6a748f5907a1e, spanId: 59a723a9f03a0017
[1fa6a748f5907a1e/59a723a9f03a0017] DEBUG [pool-1-thread-1] 40979 --- [pool-1-thread-1] c.j.b.g.s.i.InstrumentedInterceptor      : Starting gRPC call timer on endpoint: Ping/ping
[1fa6a748f5907a1e/59a723a9f03a0017] TRACE [pool-1-thread-2] 40979 --- [pool-1-thread-2] .j.b.g.s.i.DistributedTracingInterceptor : Invoking gRPC onMessage delegate with traceId: 1fa6a748f5907a1e, spanId: 59a723a9f03a0017
[1fa6a748f5907a1e/59a723a9f03a0017] TRACE [pool-1-thread-2] 40979 --- [pool-1-thread-2] .j.b.g.s.i.DistributedTracingInterceptor : Invoking gRPC onHalfClose delegate with traceId: 1fa6a748f5907a1e, spanId: 59a723a9f03a0017
[1fa6a748f5907a1e/59a723a9f03a0017] INFO  [pool-1-thread-2] 40979 --- [pool-1-thread-2] c.j.b.g.server.services.PingServiceImpl  : PING: 2e8e0739-a4bc-4b53-8a22-e66941d97330 with traceId: 1fa6a748f5907a1e, spanId: 59a723a9f03a0017
[1fa6a748f5907a1e/59a723a9f03a0017] DEBUG [pool-1-thread-2] 40979 --- [pool-1-thread-2] c.j.b.g.s.i.InstrumentedInterceptor      : Stopped gRPC call timer on endpoint: Ping/ping
[1fa6a748f5907a1e/59a723a9f03a0017] TRACE [pool-1-thread-2] 40979 --- [pool-1-thread-2] .j.b.g.s.i.DistributedTracingInterceptor : Invoking gRPC onComplete delegate with traceId: 1fa6a748f5907a1e, spanId: 59a723a9f03a0017
[1fa6a748f5907a1e/59a723a9f03a0017] INFO  [pool-1-thread-2] 40979 --- [pool-1-thread-2] c.j.b.g.s.i.ExampleCompleteInterceptor   : Example Complete Interceptor has fired ON COMPLETE with traceId: 1fa6a748f5907a1e, spanId: 59a723a9f03a0017
[f8b63f6d05088ff7/f8b63f6d05088ff7] TRACE [pool-1-thread-2] 40979 --- [pool-1-thread-2] .j.b.g.s.i.DistributedTracingInterceptor : Closing TraceContext for traceId: 1fa6a748f5907a1e, spanId: 59a723a9f03a0017
``` 