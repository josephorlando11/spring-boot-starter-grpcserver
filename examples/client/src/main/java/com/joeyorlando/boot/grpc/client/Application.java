package com.joeyorlando.boot.grpc.client;

import com.joeyorlando.boot.grpc.client.customizer.ChannelFactoryCustomizer;
import com.joeyorlando.boot.grpc.example.server.services.PingGrpc;
import com.joeyorlando.boot.grpc.example.server.services.PingMessage;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusException;
import org.slf4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.sleuth.instrument.async.LazyTraceExecutor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;

import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public ChannelFactoryCustomizer plaintextCustomizer() {
        return new ChannelFactoryCustomizer() {
            @Override
            public void customize(ChannelFactory channelFactory, ManagedChannelBuilder channelBuilder) {
                channelBuilder.usePlaintext();
            }
        };
    }

    @Bean
    public ApplicationListener<ApplicationReadyEvent> doSomethingBean() {
        return new ApplicationListener<ApplicationReadyEvent>() {

            private final Logger LOG = org.slf4j.LoggerFactory.getLogger(Application.class);

            @Autowired
            private BeanFactory beanFactory;

            @Autowired
            private PingGrpc.PingBlockingStub blockingStub;

            @Override
            public void onApplicationEvent(ApplicationReadyEvent event) {
                Executor executor = new LazyTraceExecutor(beanFactory, Executors.newSingleThreadExecutor());
                executor.execute(() -> {
                    while(true) {
                        PingMessage message = PingMessage.newBuilder().setMessage(UUID.randomUUID().toString()).build();
                        PingMessage response = blockingStub.unaryPing(message);
                        LOG.info("UnaryPing Response: {}", response.getMessage());

                        response = blockingStub.unaryPingWithResponder(message);
                        LOG.info("UnaryPingWithResponder Response: {}", response.getMessage());

                        try { blockingStub.unaryPingWithPackageAuthorizedException(message); } catch (RuntimeException e) { LOG.info("UnaryPingWithPackageAuthorizedException Response: {}", e); }
                        try { blockingStub.unaryPingWithClassAuthorizedException(message); } catch (RuntimeException e) { LOG.info("UnaryPingWithClassAuthorizedException Response: {}", e); }
                        try { blockingStub.unaryPingWithUnauthorizedException(message); } catch (RuntimeException e) { LOG.info("UnaryPingWithUnauthorizedException Response: {}", e); }

                        Iterator<PingMessage> responseStream = blockingStub.streamPing(message);
                        while (responseStream.hasNext()) {
                            LOG.info("StreamPing Response: {}", responseStream.next());
                        }
                        responseStream = blockingStub.streamPingWithResponder(message);
                        while (responseStream.hasNext()) {
                            LOG.info("StreamPingWithResponder Response: {}", responseStream.next());
                        }

                        try { blockingStub.streamPingWithPackageAuthorizedException(message); } catch (RuntimeException e) { LOG.info("StreamPingWithPackageAuthorizedException Response: {}", e); }
                        try { blockingStub.streamPingWithClassAuthorizedException(message); } catch (RuntimeException e) { LOG.info("StreamPingWithClassAuthorizedException Response: {}", e); }
                        try { blockingStub.streamPingWithUnauthorizedException(message); } catch (RuntimeException e) { LOG.info("StreamPingWithUnauthorizedException Response: {}", e); }
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
    }

    @Bean
    public PingGrpc.PingBlockingStub pingServiceBlockingStub(ChannelFactory channel) {
        return channel.registerService(PingGrpc::newBlockingStub);
    }
}