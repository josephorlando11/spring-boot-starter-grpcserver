/*
 * MIT License
 *
 * Copyright (c) 2019 Joseph Orlando
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.joeyorlando.boot.grpc.client;

import brave.Span;
import brave.Tracer;
import brave.propagation.TraceContext;
import com.joeyorlando.boot.grpc.client.interceptors.TracingClientInterceptor;
import com.joeyorlando.boot.grpc.client.internal.Constants;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ForwardingClientCall;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.annotation.Nullable;
import java.io.InputStream;
import java.util.UUID;

import static org.mockito.Mockito.doReturn;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Metadata.class, TracingClientInterceptor.class})
public class TracingInterceptorTest {

    @Mock
    private Tracer mockTracer;

    @Mock
    private Span mockSpan;

    private TraceContext mockTraceContext;

    private long traceId;

    private long spanId;

    @InjectMocks
    private TracingClientInterceptor tracingInterceptor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        traceId = UUID.randomUUID().getMostSignificantBits();
        spanId = UUID.randomUUID().getLeastSignificantBits();
        mockTraceContext = TraceContext.newBuilder()
                .traceId(traceId)
                .spanId(spanId)
                .build();
        doReturn(mockTraceContext).when(mockSpan).context();
        doReturn(mockSpan).when(mockTracer).currentSpan();
    }

    @Test
    public void appliesTracingHeaders() {
        MethodDescriptor.Marshaller marshaller = new MethodDescriptor.Marshaller<Object>() {
            @Override
            public InputStream stream(Object value) {
                return null;
            }
            @Override
            public Object parse(InputStream stream) {
                return null;
            }
        };

        MethodDescriptor methodDescriptor = MethodDescriptor.newBuilder()
                .setFullMethodName("MockFullMethodName")
                .setType(MethodDescriptor.MethodType.UNKNOWN)
                .setRequestMarshaller(marshaller)
                .setResponseMarshaller(marshaller)
                .build();
        CallOptions callOptions = CallOptions.DEFAULT;
        Channel channel = new Channel() {
            @Override
            public <RequestT, ResponseT> ClientCall<RequestT, ResponseT> newCall(MethodDescriptor<RequestT, ResponseT> methodDescriptor, CallOptions callOptions) {
                return new ForwardingClientCall<RequestT, ResponseT>() {
                    @Override
                    protected ClientCall<RequestT, ResponseT> delegate() {
                        return new ClientCall<RequestT, ResponseT>() {
                            @Override
                            public void start(Listener<ResponseT> responseListener, Metadata headers) {

                            }

                            @Override
                            public void request(int numMessages) {

                            }

                            @Override
                            public void cancel(@Nullable String message, @Nullable Throwable cause) {

                            }

                            @Override
                            public void halfClose() {

                            }

                            @Override
                            public void sendMessage(RequestT message) {

                            }
                        };
                    }
                };
            }

            @Override
            public String authority() {
                return null;
            }
        };

        ClientCall.Listener responseListener = Mockito.mock(ClientCall.Listener.class);

        Metadata responseMetadata = PowerMockito.spy(new Metadata());
        ClientCall callBack = tracingInterceptor.interceptCall(methodDescriptor, callOptions, channel);
        callBack.start(responseListener, responseMetadata);

        Mockito.verify(responseMetadata, Mockito.times(1)).put(Constants.CLIENT_METADATA_TRACINGID, String.valueOf(traceId));
        Mockito.verify(responseMetadata, Mockito.times(1)).put(Constants.CLIENT_METADATA_SPANID, String.valueOf(spanId));
    }
}
